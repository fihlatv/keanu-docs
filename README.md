# Keanu Documentation

**[docs.keanu.im](http://docs.keanu.im)**

## Setup

Needs mkdocs and a few other deps on the system and install with `pip install
-U -r requirements.txt`. Use a virtualenv if you wish.

### Preview docs locally

```bash
# preview mkdocs
mkdocs serve
```

Then open your browser to: http://127.0.0.1:8000/

### Publishing

Deployment to [docs.keanu.im](http://docs.keanu.im) happens automatically from the master branch.

## Copyright / License

Copyright 2019 [The Guardian Project](https://guardianproject.info).

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
